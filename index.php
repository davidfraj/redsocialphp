<?php  
//Realizamos requires
require('classes/class.conexion.php');
require('classes/class.form.php');
require('classes/class.menu.php');

require('classes/class.post.php');
require('classes/class.noticia.php');
require('classes/class.video.php');

require('classes/class.repositorio.php');

require('classes/class.posts.php');
require('classes/class.noticias.php');
require('classes/class.videos.php');


//Recojo la pagina que quiero cargar en la parte central
if(isset($_GET['p'])){
  $p=$_GET['p'];
}else{
  $p='inicio.php';
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Proyecto BootStrap 01</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

  </head>
  <body>
    <section class="container">
      <div class="row">
        <nav class="col-sm-3"><?php require('includes/menui.php'); ?></nav>
        <section class="col-sm-6"><?php require('paginas/'.$p); ?></section>
        <nav class="col-sm-3"><?php require('includes/menud.php'); ?></nav> 
      </div>
    </section> 


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

  </body>
</html>