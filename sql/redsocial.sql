-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-06-2017 a las 15:52:27
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `redsocial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `autor` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `contenido` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `autor`, `contenido`, `fecha`) VALUES
(2, 'Hoy es viernes', 'David Fraj Blesa', '<div id="TXT1_mobile">\r\n<p>Los equipos de Bomberos, Protecci&oacute;n Civil y la Guardia Civil han rescatado&nbsp;a los dos alba&ntilde;iles que hab&iacute;an quedado atrapados en <a href="http://www.heraldo.es/tags/lugares/nuevalos.html" target="_blank" rel="noopener">Nu&eacute;valos</a>&nbsp;tras el derrumbe de una vivienda&nbsp;en la que se encontraban trabajando. Seg&uacute;n ha informado la Diputaci&oacute;n de Zaragoza, los dos hombres, de 39 y 49 a&ntilde;os, han resultado <strong>heridos de gravedad&nbsp;</strong>y su estado de salud est&aacute; siendo evaluado&nbsp;por personal sanitario para proceder a su traslado a centros hospitalarios, ha informado la Guardia Civil.</p>\r\n</div>\r\n<p>Gran parte de <strong>una vivienda de dos plantas que estaba en obras se ha desplomado este viernes por la ma&ntilde;ana atrapando a dos alba&ntilde;iles que se encontraban trabajando en su interior</strong>, tal y como ha confirmado el alcalde, Manuel Peir&oacute;, propietario de la casa, minutos despu&eacute;s de que tuviera lugar el derrumbe, en torno a las 11.00.</p>', '2017-06-09 17:51:34'),
(3, 'Rescatados los dos albañiles', 'Heraldo.es', '<p>La Guardia Civil&nbsp;ha informado de&nbsp;que <strong>los dos alba&ntilde;iles atrapados han permanecido conscientes en todo momento</strong>. De hecho, han podido ser&nbsp;asistidos por personal sanitario antes incluso de completar su&nbsp;liberaci&oacute;n. <a href="http://videos.heraldo.es/aragon/eduardo-sanchez-estaban-atrapados-bajo-los-escombros-de-dos-plantas-yjEK3Q/" target="_blank" rel="noopener">Eduardo S&aacute;nchez, jefe de Bomberos de la DPZ</a>, ha explicado que los trabajadores<strong> hab&iacute;an quedado sepultados bajo las dos plantas </strong>del edificio, por lo que primero se ha procedido al <strong>desescombro vertical</strong> hasta llegar a liberarles&nbsp;la cabeza y el torso para que los servicios m&eacute;dicos pudiesen&nbsp;acceder a ellos y estabilizarlos. Tras ponerles una v&iacute;a y controlar sus constantes vitales, los equipos de rescate han procedido a liberar tambi&eacute;n las piernas de entre los escombros y los alba&ntilde;iles han podido ser rescatados y atendidos&nbsp;por los equipos sanitarios desplazados hasta all&iacute; en&nbsp;<strong>veh&iacute;culos&nbsp;medicalizados</strong> -un helic&oacute;ptero y dos ambulancias-.</p>\r\n<div id="TXT2_to_6_mobile" style="opacity: 1;">\r\n<p>Visiblemente afectado y nervioso por el suceso, el alcalde de Nu&eacute;valos ha reconocido que en el inmueble se estaban ejecutando unos trabajos para reforzar la estructura que &ldquo;se estaba hundiendo&rdquo;. El regidor explica que <strong>una de las causas que se barajan del derrumbe podr&iacute;an ser las &uacute;ltimas lluvias</strong> registradas en la comarca de la Comunidad de Calatayud.</p>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>El edificio se encuentra en el n&uacute;mero 61 de la calle de la ermita de San Sebasti&aacute;n, muy cerca de la carretera que lleva al Monasterio de Piedra y&nbsp;la zona derrumbada se corresponde con la parte de una antigua nave que fue rehabilitada hace unos a&ntilde;os como vivienda.&nbsp;Algunos testigos presenciales han corroborado que la zona habitable&nbsp;se ha desplomado&nbsp;casi por completo: <strong>"Solo ha quedado un muro en pie</strong>", aseveraba un vecino minutos despu&eacute;s del derrumbe. </p>', '2017-06-09 17:57:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `titulo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `contenido` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `titulo`, `contenido`, `fecha`) VALUES
(5, 'ccc2', 'ccc2', '0000-00-00 00:00:00'),
(7, 'ddd', 'dddd', '2017-06-08 16:48:59'),
(8, 'eee', 'eee', '2017-06-08 16:49:02'),
(10, 'gggg2', 'gggg2', '2017-06-08 16:49:14'),
(11, 'aasdasd22', '<p>asdasdasd222</p>', '2017-06-08 19:03:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `titulo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `autor` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `enlace` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `autor`, `enlace`, `fecha`) VALUES
(1, 'Peliculas que pueden interesarte', 'youtube.com', 'https://www.youtube.com/embed/yJGVLr0Dgng', '2017-06-09 19:55:55'),
(2, 'Video de ciencia ficcion', 'Youtube.com', 'https://www.youtube.com/embed/Ut0SXRC0Kcw', '2017-06-09 19:56:28');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
