<?php  
//Fichero classes/class.videos.php

class Videos extends Repositorio{

	///////////////////////////////////////////////////
	//////////////// METODO LISTADO
	///////////////////////////////////////////////////
	public function listado(){
		
		$consulta=parent::listado();

		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new Video($fila);
		}

		$resultado='';
		foreach ($this->elementos as $elemento) {
			$resultado.='<article>';
			$resultado.='<header>';
			$resultado.='<h2>';
			$resultado.=$elemento->getTitulo();
			$resultado.='<small>';

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elemento->getId().'">Ver</a>';
			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elemento->getId().'" onClick="if(!confirm(\'Estas seguro\')){return false;};">Borrar</a>';
			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elemento->getId().'">Modificar</a>';

			$resultado.='</small>';
			$resultado.='</h2>';
			$resultado.='</header>';

			$resultado.='<section>';
			$resultado.='<iframe width="560" height="315" src="'.$elemento->getEnlace().'" frameborder="0" allowfullscreen></iframe>';
			$resultado.='</section>';

			$resultado.='<footer>';
			$resultado.=$elemento->getAutor();
			$resultado.='</footer>';

			$resultado.='</article>';
		}
		$resultado.='<a href="index.php?p='.$this->fich.'&accion=insertar">Insertar</a>';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO VER
	///////////////////////////////////////////////////
	public function ver($id){

		$fila=parent::ver($id);

		$video=new Video($fila);
		$resultado='';
		$resultado.='
			<article>
			<header>'.$video->getTitulo().'</header>
			<section>
			<iframe width="560" height="315" src="'.$video->getEnlace().'" frameborder="0" allowfullscreen></iframe>
			</section>
			<footer>'.$video->getFecha().'</footer>	
			</article>
			';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERTAR
	///////////////////////////////////////////////////
	public function insertar(){

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=insercion');
		$r.=Form::label('Titulo:');
		$r.=Form::texto('titulo');

		$r.=Form::label('Enlace:');
		$r.=Form::texto('enlace');

		$r.=Form::label('Autor:');
		$r.=Form::texto('autor');

		$r.=Form::submit('insertar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERCION
	///////////////////////////////////////////////////
	public function insercion(){
		$titulo=$_POST['titulo'];
		$autor=$_POST['autor'];
		$enlace=$_POST['enlace'];
		$fecha=Date('Y-m-d H:i:s');
		$sql="INSERT INTO $this->tabla(titulo, autor, enlace, fecha) VALUES ('$titulo', '$autor', '$enlace', '$fecha')";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Insertado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICAR
	///////////////////////////////////////////////////
	public function modificar($id){

		$fila=parent::modificar($id);
		$video=new Video($fila);

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=modificacion');
		$r.=Form::label('Titulo:');
		$r.=Form::texto('titulo', $video->getTitulo());

		$r.=Form::label('Enlace:');
		$r.=Form::texto('enlace', $video->getEnlace());

		$r.=Form::label('Autor:');
		$r.=Form::texto('autor', $video->getAutor());

		$r.=Form::hidden('id', $video->getId());
		$r.=Form::submit('guardar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICACION
	///////////////////////////////////////////////////
	public function modificacion(){
		
		$titulo=$_POST['titulo'];
		$autor=$_POST['autor'];
		$enlace=$_POST['enlace'];
		$id=$_POST['id'];

		$sql="UPDATE $this->tabla SET titulo='$titulo', autor='$autor', enlace='$enlace' WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Modificado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

}

?>