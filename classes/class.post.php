<?php  
//Fichero class.post.php

class Post{
	
	private $id;
	private $titulo;
	private $contenido;
	private $fecha;

	public function __construct($fila){
		$this->id=$fila['id'];
		$this->titulo=$fila['titulo'];
		$this->contenido=$fila['contenido'];
		$this->fecha=$fila['fecha'];
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function getContenido(){
		return $this->contenido;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function getId(){
		return $this->id;
	}

}

?>