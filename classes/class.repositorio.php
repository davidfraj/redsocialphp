<?php  
//Fichero classes/class.repositorio.php

class Repositorio{

	protected $conexion;
	protected $elementos;
	protected $tabla;
	protected $fich;

	public function __construct($tabla, $fich){
		$this->conexion=Conexion::$conexion;
		$this->elementos=[];
		$this->tabla=$tabla; //Lo relleno mediante el constructor
		$this->fich=$fich; //Lo relleno mediante el constructor
	}

	///////////////////////////////////////////////////
	//////////////// METODO LISTADO
	///////////////////////////////////////////////////
	public function listado(){
		
		$sql="SELECT * FROM $this->tabla ORDER BY id DESC";
		//Devuelve un objeto de la Class Mysqli_result
		$consulta=$this->conexion->query($sql); 
		
		return $consulta;
		
	}

	///////////////////////////////////////////////////
	//////////////// METODO VER
	///////////////////////////////////////////////////
	public function ver($id){
		$sql="SELECT * FROM $this->tabla WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		
		return $fila;
	}

	///////////////////////////////////////////////////
	//////////////// METODO BORRAR
	///////////////////////////////////////////////////
	public function borrar($id){
		$sql="DELETE FROM $this->tabla WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Borrado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error al borrar';
		}
	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERTAR
	///////////////////////////////////////////////////
	public function insertar(){

		//Se encargara su hija

	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERCION
	///////////////////////////////////////////////////
	public function insercion(){
		
		//Se encargara su hija

	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICAR
	///////////////////////////////////////////////////
	public function modificar($id){

		$sql="SELECT * FROM $this->tabla WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		
		return $fila;

	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICACION
	///////////////////////////////////////////////////
	public function modificacion(){
		
		//Se encargara su hija

	}

	///////////////////////////////////////////////////
	//////////////// METODO ACCIONES
	///////////////////////////////////////////////////
	public function acciones(){
		//Recojo la accion pasada por el usuario
		if(isset($_GET['accion'])){
			$accion=$_GET['accion'];
		}else{
			$accion='listado';
		}

		//Realizo un switch por las diferentes acciones
		switch($accion){
			case 'ver':
				$id=$_GET['id'];
				return $this->ver($id);
				break;
			case 'insertar':
				return $this->insertar();
				break;
			case 'insercion':
				return $this->insercion();
				break;
			case 'modificar':
				$id=$_GET['id'];
				return $this->modificar($id);
				break;
			case 'modificacion':
				return $this->modificacion();
				break;
			case 'borrar':
				$id=$_GET['id'];
				return $this->borrar($id);
				break;
			case 'listado':
			default:
				return $this->listado();
				break;
		}
	}
}

?>