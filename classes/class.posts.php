<?php  
//Fichero classes/class.posts.php

class Posts extends Repositorio{

	///////////////////////////////////////////////////
	//////////////// METODO LISTADO
	///////////////////////////////////////////////////
	public function listado(){
		
		$consulta=parent::listado();

		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new Post($fila);
		}

		$resultado='';
		foreach ($this->elementos as $elemento) {
			$resultado.='<article>';
			$resultado.=$elemento->getTitulo();

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elemento->getId().'">Ver</a>';

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elemento->getId().'" onClick="if(!confirm(\'Estas seguro\')){return false;};">Borrar</a>';

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elemento->getId().'">Modificar</a>';

			$resultado.='</article>';
		}
		$resultado.='<a href="index.php?p='.$this->fich.'&accion=insertar">Insertar</a>';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO VER
	///////////////////////////////////////////////////
	public function ver($id){

		$fila=parent::ver($id);

		$post=new Post($fila);
		$resultado='';
		$resultado.='
			<article>
			<header>'.$post->getTitulo().'</header>
			<section>'.$post->getContenido().'</section>
			<footer>'.$post->getFecha().'</footer>	
			</article>
			';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERTAR
	///////////////////////////////////////////////////
	public function insertar(){

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=insercion');
		$r.=Form::label('Titulo:');
		$r.=Form::texto('titulo');
		$r.=Form::label('Contenido:');
		$r.=Form::areatexto('contenido');
		$r.=Form::submit('insertar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERCION
	///////////////////////////////////////////////////
	public function insercion(){
		$titulo=$_POST['titulo'];
		$contenido=$_POST['contenido'];
		$fecha=Date('Y-m-d H:i:s');
		$sql="INSERT INTO $this->tabla(titulo, contenido, fecha) VALUES ('$titulo', '$contenido', '$fecha')";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Insertado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICAR
	///////////////////////////////////////////////////
	public function modificar($id){

		$fila=parent::modificar($id);
		$post=new Post($fila);

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=modificacion');
		$r.=Form::label('Titulo:');
		$r.=Form::texto('titulo', $post->getTitulo());
		$r.=Form::label('Contenido:');
		$r.=Form::areatexto('contenido', $post->getContenido());
		$r.=Form::hidden('id', $post->getId());
		$r.=Form::submit('guardar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICACION
	///////////////////////////////////////////////////
	public function modificacion(){
		
		$titulo=$_POST['titulo'];
		$contenido=$_POST['contenido'];
		$id=$_POST['id'];

		$sql="UPDATE $this->tabla SET titulo='$titulo', contenido='$contenido' WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Modificado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

}

?>