<?php  
//Fichero class.noticia.php

class Noticia{
	
	private $id;
	private $titulo;
	private $autor;
	private $contenido;
	private $fecha;

	public function __construct($fila){
		$this->id=$fila['id'];
		$this->titulo=$fila['titulo'];
		$this->autor=$fila['autor'];
		$this->contenido=$fila['contenido'];
		$this->fecha=$fila['fecha'];
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function getContenido(){
		return $this->contenido;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function getId(){
		return $this->id;
	}

	public function getAutor(){
		return $this->autor;
	}

}

?>