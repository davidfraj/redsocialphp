<?php  
//fichero   classes/class.menu.php
class Menu{

	private $enlaces;
	private $nombres;
	private $alineacion;
	private $titulo;

	public function __construct($enl, $nom){
		$this->enlaces=$enl;
		$this->nombres=$nom;
		$this->alineacion='text-left';
		$this->titulo='';
	}

	public function pintar(){
		global $p;
		$resultado='';
		if(strlen($this->titulo)>0){
			$resultado.='<h4 class="'.$this->alineacion.'">'.$this->titulo.'</h4>';
		}
		$resultado.='<ul class="nav nav-pills nav-stacked">';
		for ($i=0; $i < count($this->enlaces); $i++) {
		  if($p==$this->enlaces[$i]){
		  	$c='active';
		  }else{
		  	$c='';
		  }
		  $resultado.='<li class="'.$c.' '.$this->alineacion.'"><a href="index.php?p='.$this->enlaces[$i].'">'.$this->nombres[$i].'</a></li>';
		}
		$resultado.='</ul>';
		return $resultado;
	}

	public function setAlineacion($ali){
		switch($ali){
			case 'derecha':
				$this->alineacion='text-right';
				break;
			case 'centro':
				$this->alineacion='text-center';
				break;
			default:
				$this->alineacion='text-left';
		}
	}

	public function setTitulo($tit){
		$this->titulo=$tit;
	}
}
?>





