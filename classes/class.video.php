<?php  
//Fichero class.video.php

class Video{
	
	private $id;
	private $titulo;
	private $enlace;
	private $autor;
	private $fecha;

	public function __construct($fila){
		$this->id=$fila['id'];
		$this->titulo=$fila['titulo'];
		$this->enlace=$fila['enlace'];
		$this->autor=$fila['autor'];
		$this->fecha=$fila['fecha'];
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function getEnlace(){
		return $this->enlace;
	}

	public function getAutor(){
		return $this->autor;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function getId(){
		return $this->id;
	}

}

?>