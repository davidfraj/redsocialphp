<?php  
//Fichero classes/class.noticias.php

class Noticias extends Repositorio{

	///////////////////////////////////////////////////
	//////////////// METODO LISTADO
	///////////////////////////////////////////////////
	public function listado(){
		
		$consulta=parent::listado();

		while($fila=$consulta->fetch_array()){
			$this->elementos[]=new Noticia($fila);
		}

		$resultado='';
		foreach ($this->elementos as $elemento) {
			$resultado.='<article>';
			$resultado.='<header>';
			$resultado.='<h2>';
			$resultado.=$elemento->getTitulo();
			$resultado.='<small>';
			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=ver&id='.$elemento->getId().'">Ver</a>';

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=borrar&id='.$elemento->getId().'" onClick="if(!confirm(\'Estas seguro\')){return false;};">Borrar</a>';

			$resultado.=' - <a href="index.php?p='.$this->fich.'&accion=modificar&id='.$elemento->getId().'">Modificar</a>';
			$resultado.='</small>';
			$resultado.='</h2>';
			$resultado.='</header>';

			$resultado.='<section>';
			$resultado.=$elemento->getContenido();
			$resultado.='</section>';

			$resultado.='<footer>';
			$resultado.=$elemento->getFecha();
			$resultado.='</footer>';

			$resultado.='</article>';
		}
		$resultado.='<a href="index.php?p='.$this->fich.'&accion=insertar">Insertar</a>';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO VER
	///////////////////////////////////////////////////
	public function ver($id){

		$fila=parent::ver($id);

		$noticia=new Noticia($fila);
		$resultado='';
		$resultado.='
			<article>
			<header>'.$noticia->getTitulo().'</header>
			<section>'.$noticia->getAutor().'</section>
			<section>'.$noticia->getContenido().'</section>
			<footer>'.$noticia->getFecha().'</footer>	
			</article>
			';
		return $resultado;
	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERTAR
	///////////////////////////////////////////////////
	public function insertar(){

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=insercion');
		$r.=Form::label('Titulo de la noticia:');
		$r.=Form::texto('titulo');
		$r.=Form::label('Autor de la noticia:');
		$r.=Form::texto('autor');
		$r.=Form::label('Contenido de la noticia:');
		$r.=Form::areatexto('contenido');
		$r.=Form::submit('insertar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO INSERCION
	///////////////////////////////////////////////////
	public function insercion(){
		$titulo=$_POST['titulo'];
		$autor=$_POST['autor'];
		$contenido=$_POST['contenido'];
		$fecha=Date('Y-m-d H:i:s');
		$sql="INSERT INTO $this->tabla(titulo, contenido, fecha, autor) VALUES ('$titulo', '$contenido', '$fecha', '$autor')";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Insertado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICAR
	///////////////////////////////////////////////////
	public function modificar($id){

		$fila=parent::modificar($id);
		$noticia=new Noticia($fila);

		$r=Form::inicio('index.php?p='.$this->fich.'&accion=modificacion');
		$r.=Form::label('Titulo:');
		$r.=Form::texto('titulo', $noticia->getTitulo());
		$r.=Form::label('Autor:');
		$r.=Form::texto('autor', $noticia->getAutor());
		$r.=Form::label('Contenido:');
		$r.=Form::areatexto('contenido', $noticia->getContenido());
		$r.=Form::hidden('id', $noticia->getId());
		$r.=Form::submit('guardar');
		$r.=Form::fin();
		return $r;

	}

	///////////////////////////////////////////////////
	//////////////// METODO MODIFICACION
	///////////////////////////////////////////////////
	public function modificacion(){
		
		$titulo=$_POST['titulo'];
		$contenido=$_POST['contenido'];
		$autor=$_POST['autor'];
		$id=$_POST['id'];

		$sql="UPDATE $this->tabla SET titulo='$titulo', autor='$autor',contenido='$contenido' WHERE id=$id";
		$consulta=$this->conexion->query($sql);
		if($consulta==true){
			//return 'Modificado con exito';
			header('Location:index.php?p='.$this->fich);
		}else{
			return 'Error';
		}
	}

}

?>