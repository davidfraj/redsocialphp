<?php  
//Fichero includes/menui.php

$enlaces=array('inicio.php', 'noticias.php', 'videos.php', 'usuarios.php', 'contacto.php', 'comentarios.php');
$nombres=array('Inicio', 'Noticias', 'Videos de usuarios', 'Usuarios', 'Contactanos', 'Ultimos comentarios');

$menui=new Menu($enlaces, $nombres);
$menui->setAlineacion('izquierda'); //izquierda derecha centro
$menui->setTitulo('Menu principal');
echo $menui->pintar();

?>